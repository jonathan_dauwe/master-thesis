import sympy


def find_prime_number(sieving, L, R, pte_solution):
    """
    If there is a positive integer l in [L, R) such that:
        - a(l) is B-smooth ;
        - b(l) is B-smooth ;
        - 2*a(l) + 1 is a prime number ;
    then this function returns the prime number 2*a(l) + 1.
    Otherwise it returns an ValueError.
    """
    a, b, C = pte_solution.a, pte_solution.b, pte_solution.C
    interval = [n for n in range(C) if a(n) % C == 0]
    # for all n in interval, a(n) is divisible by C
    maxi = max(pte_solution.A + pte_solution.B)
    mask = [((maxi - i) in pte_solution.elements) for i in range(maxi + 1)]
    for i in range((R - L) // C):
        for j in interval:
            # For each l such that C divides a(l), we check if l-i is B-smooth
            # (where i is an element of a PTE solution's vector).
            l = i * C + j
            if sieving[l - maxi:l + 1] == mask:
                m = b(L+l) // C
                p = 2 * m + 1
                if sympy.isprime(p):
                    return p
    raise ValueError("Impossible to find a prime number with these arguments.")
