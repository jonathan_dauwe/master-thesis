from pte_solution import PTE_Solution

""" Here is a list. The i-th element of this list is a list of solutions to PTE problems of degree i. """

PTE_SOLUTIONS = (
    [  # Order 0
        PTE_Solution([], [])
    ],
    [  # Order 1
        PTE_Solution([1], [0]),
        PTE_Solution([2], [0]),
        PTE_Solution([2], [1])
    ],
    [  # Order 2
        PTE_Solution([1, 1], [0, 2]),
        PTE_Solution([1, 2], [0, 3]),
        # PTE_Solution([2,2],[0,4]), -> [1,1] = [0,2] (*2)
        PTE_Solution([1, 3], [0, 4]),
        PTE_Solution([2, 3], [0, 5]),
        PTE_Solution([1, 4], [0, 5]),
        # PTE_Solution([3,3],[0,6]), -> [1,1] = [0,2] (*3)
        # PTE_Solution([2,4],[0,6]), -> [1,2] = [0,3] (*2)
        PTE_Solution([1, 5], [0, 6])
    ],
    [  # Order 3
        PTE_Solution([0, 3, 3], [1, 1, 4]),
        PTE_Solution([0, 4, 5], [1, 2, 6])
        # PTE_Solution([1, 4, 4], [2, 2, 5]) is incorrect because there is no 0.
        # If we substract 1 to all the elements, we have the first solution.
    ],
    [  # Order 4
        PTE_Solution([0, 3, 4, 7], [1, 1, 6, 6]),
        PTE_Solution([0, 4, 7, 11], [1, 2, 9, 10]),
        PTE_Solution([0, 5, 5, 10], [1, 2, 8, 9]),
        PTE_Solution([0, 5, 10, 15], [1, 3, 12, 14]),
        PTE_Solution([0, 6, 7, 13], [1, 3, 10, 12]),
        PTE_Solution([0, 6, 13, 19], [1, 4, 15, 18]),
        PTE_Solution([0, 7, 9, 16], [1, 4, 12, 15]),
        PTE_Solution([0, 8, 11, 19], [1, 5, 14, 18]),
        PTE_Solution([0, 5, 12, 17], [2, 2, 15, 15]),
        PTE_Solution([0, 6, 8, 14], [2, 2, 12, 12]),
        PTE_Solution([0, 7, 11, 18], [2, 3, 15, 16]),
        PTE_Solution([0, 8, 9, 17], [2, 3, 14, 15])
    ],
    [  # Order 5
        PTE_Solution([0, 4, 8, 16, 17], [1, 2, 10, 14, 18])
    ],
    [  # Order 6
        PTE_Solution([0, 3, 5, 11, 13, 16], [1, 1, 8, 8, 15, 15]),
        PTE_Solution([0, 5, 6, 16, 17, 22], [1, 2, 10, 12, 20, 21]),
        PTE_Solution([0, 4, 9, 17, 22, 26], [1, 2, 12, 14, 24, 25]),
        PTE_Solution([0, 7, 7, 21, 21, 28], [1, 3, 12, 16, 25, 27]),
        PTE_Solution([0, 7, 8, 22, 23, 30], [2, 2, 15, 15, 28, 28]),
        PTE_Solution([0, 5, 13, 23, 31, 36], [1, 3, 16, 20, 33, 35]),
        PTE_Solution([0, 8, 9, 25, 26, 34], [1, 4, 14, 20, 30, 33]),
        PTE_Solution([0, 7, 11, 25, 29, 36], [1, 4, 15, 21, 32, 35]),
        PTE_Solution([0, 9, 11, 29, 31, 40], [1, 5, 16, 24, 35, 39]),
        PTE_Solution([0, 8, 11, 27, 30, 38], [2, 3, 18, 20, 35, 36]),
        PTE_Solution([0, 5, 16, 26, 37, 42], [2, 2, 21, 21, 40, 40]),
        PTE_Solution([0, 6, 17, 29, 40, 46], [1, 4, 20, 26, 42, 45]),
        PTE_Solution([0, 7, 14, 28, 35, 42], [2, 3, 20, 22, 39, 40]),
        PTE_Solution([0, 10, 13, 33, 36, 46], [1, 6, 18, 28, 40, 45]),
        PTE_Solution([0, 9, 17, 34, 36, 46], [1, 6, 24, 25, 42, 44]),
        PTE_Solution([0, 9, 14, 32, 37, 46], [2, 4, 21, 25, 42, 44]),
        PTE_Solution([0, 9, 16, 34, 41, 50], [1, 6, 20, 30, 44, 49]),
        PTE_Solution([0, 11, 15, 37, 41, 52], [1, 7, 20, 32, 45, 51]),
        PTE_Solution([0, 7, 21, 35, 49, 56], [1, 5, 24, 32, 51, 55]),
        PTE_Solution([0, 12, 13, 37, 38, 50], [2, 5, 22, 28, 45, 48])
    ]
)
