/* Return true if the vector contains the element and false otherwise. */
bool contains(std::vector<int> v, int element){
    return std::count(v.begin(), v.end(), element);
}

/* Return true if the vector v1 contains all the elements of the vector v2. */
template <typename T>
bool is_included(const std::vector<T>& v1, const std::vector<T>& v2){
    return std::all_of(v1.cbegin(), v1.cend(), [&v2](T i){ return contains(v2, i); });
}

/* Return an array of a given size initialized with a given value. */
template <typename T>
T* initialize(size_t size, T value){
    auto *res = (T*) malloc(sizeof(T) * size);
    for (int i = 0; i < size - 1; i++) {
        res[i] = value;
    }
    res[size - 1] = '\0';
    return res;
}

/* TODO: ancient name is concatenate */
/* Return the sorted disjoint union of two vectors with the same size. */
template <typename T>
std::vector<T> disjoint_union(const std::vector<T>& v1, const std::vector<T>& v2){
    std::vector<T> res;
    typename std::vector<T>::iterator it = res.begin();
    for (auto elem: v1){
        if (!contains(res, elem)){
            res.push_back(elem);
        }
    }
    for (auto elem: v2){
        if (!contains(res, elem)){
            res.push_back(elem);
        }
    }
    std::sort(res.begin(), res.end());
    return res;
}

/* TODO: remove it. */
template <typename T>
void print(std::vector<T> vector){
    for (auto elem: vector){
        std::cout << elem << " ";
    }
    std::cout << "\n";
}