#include "ordinary_tree.cpp"

#ifndef HEADER_PRIME
#define HEADER_PRIME
#include "../prime/prime_tools.cpp"
#endif

typedef std::pair<std::vector<PTE_Solution>, std::vector<PTE_Solution>> pair_pte_sol;

pair_pte_sol solutions_repartition(const std::vector<PTE_Solution>& pte_solutions, int element){
    std::vector<PTE_Solution> with, without;
    for (auto & sol: pte_solutions){
        if (contains(sol.elements, element)){
            with.push_back(sol);
        }else{
            without.push_back(sol);
        }
    }
    pair_pte_sol res(with, without);
    return res;
}

/* TODO: trier les elements dans pte_solutions pour pouvoir choisir le plus petit en cas d'égalité sur l'élément le plus présent */
OrdinaryTree* original_tree_maker(const std::vector<PTE_Solution>& pte_solutions, std::vector<int> used_elements, int common_element){
    auto* res = new OrdinaryTree;
    res->set_value(common_element);
    used_elements.push_back(common_element);
    if ((pte_solutions.size() == 1) and (pte_solutions.at(0).elements.size() == used_elements.size())){
        res->set_solutions(pte_solutions);
        //std::cout << " / ";// TODO: remove it
        return res;
    }
    std::vector<PTE_Solution> resting_solutions;
    resting_solutions.assign(pte_solutions.begin(), pte_solutions.end());
    std::vector<PTE_Solution> solutions_with;
    while (!resting_solutions.empty()){
        int most_common_element = most_common(resting_solutions, used_elements);
        pair_pte_sol repartition = solutions_repartition(resting_solutions, most_common_element);
        solutions_with = repartition.first;
        resting_solutions = repartition.second;
        //std::cout << most_common_element << " ";// TODO: remove it
        res->add_child(original_tree_maker(solutions_with, used_elements, most_common_element));
        //used_elements.pop_back();
        //std::cout << "|";// TODO: remove it
        // TODO: Verify I can remove most_common_element from used_elements to increase the speed.
    }
    //std::cout << (res->get_children()).size() << " "; // TODO: remove it
    return res;
}

OrdinaryTree* original_tree_maker(const std::vector<PTE_Solution>& pte_solutions){
    std::vector<int> used_elements;
    return original_tree_maker(pte_solutions, used_elements, 0);
}

big_int original_get_prime_from_l(const big_int* sieving, OrdinaryTree* tree, big_int l, big_int L){
    std::vector<OrdinaryTree*> nodes = {tree};
    while (!nodes.empty()){
        std::vector<OrdinaryTree*> new_nodes;
        for (auto node: nodes){
            for (const auto& sol : node->get_solutions()){
                big_int p = 2*sol.mC(l+L+1)/sol.C + 1;
                if ((sol.mC(l+L+1) % sol.C == 0) and (is_prime(p))){
                    return p;
                }
            }
            int i = node->get_value();
            if (l >= i and sieving[l-i]){
                new_nodes.insert(new_nodes.end(), (node->get_children()).begin(), (node->get_children()).end());
            }
        }
        nodes = new_nodes;
        new_nodes.clear();
    }
    return 0;
}