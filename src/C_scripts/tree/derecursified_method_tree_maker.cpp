#ifndef HEADER_BIN_TREE
#define HEADER_BIN_TREE
#include "binary_tree.cpp"
#endif

#ifndef HEADER_PRIME
#define HEADER_PRIME
#include "../prime/prime_tools.cpp"
#endif

BinaryTree* derecursified_tree_maker(const std::vector<PTE_Solution>& pte_solutions, std::vector<int> used_elements, BinaryTree* uncle){  // Classer les pte_solutions en fonction de leur élément maximal (au début) !
    auto* res = new BinaryTree;
    res->set_value(-1);
    std::vector<PTE_Solution> solutions_not_found;
    for (const PTE_Solution& sol: pte_solutions){
        if (is_included(sol.elements, used_elements)){
            res->add_solution(sol); // TODO: repartition solutions vs not_found_solutions
        }
        else {
            solutions_not_found.push_back(sol);
        }
    }
    if (solutions_not_found.empty()){
        res->set_right_child(uncle->get_right_child());
        res->set_left_child(uncle->get_left_child());
        res->set_value(uncle->get_value());
        return res;
    }
    int most_common_element = most_common(solutions_not_found, used_elements);
    res->set_value(most_common_element);
    std::vector<PTE_Solution> solutions_without;
    std::vector<PTE_Solution> solutions_with;
    for (auto & pte_solution : pte_solutions){
        if (!contains(pte_solution.elements, most_common_element)){
            solutions_without.push_back(pte_solution);
        }
        else {solutions_with.push_back(pte_solution);}
    }
    BinaryTree* right_child = derecursified_tree_maker(solutions_without, used_elements, uncle);
    res->set_right_child(right_child);
    used_elements.push_back(most_common_element);
    BinaryTree* left_child = derecursified_tree_maker(solutions_with, used_elements, right_child);
    res->set_left_child(left_child);
    return res;
}

BinaryTree* derecursified_tree_maker(const std::vector<PTE_Solution>& pte_solutions){
    std::vector<int> elements_in;
    auto* empty_node = new BinaryTree;
    empty_node->set_value(-1);
    return derecursified_tree_maker(pte_solutions, elements_in, empty_node);
}

big_int derecursified_get_prime_from_l(const big_int* sieving, BinaryTree* tree, big_int l, big_int L){
    while (tree->get_value() != -1){
        if (!tree->get_solutions().empty()){ // TODO: Is this block is needed? 'cause last node is mandatory a leaf and then value = -1.
            for (const auto& sol : tree->get_solutions()){
                big_int p = 2*sol.mC(l+L+1)/sol.C + 1;
                if ((sol.mC(l+L+1) % sol.C == 0) and (is_prime(p))){
                    return p;
                }
            }
        }
        int i = tree->get_value();
        if (l >= i and sieving[l-i]){
            tree = tree->get_left_child();
        }
        else{
            tree = tree->get_right_child();
        }
    }
    if (!tree->get_solutions().empty()){
        for (const auto& sol : tree->get_solutions()){
            big_int p = 2*sol.mC(l+L+1)/sol.C + 1;
            if ((sol.mC(l+L+1) % sol.C == 0) and (is_prime(p))){
                return p;
            }
        }
    }
    return 0;
}