#ifndef HEADER_BIN_TREE
#define HEADER_BIN_TREE
#include "binary_tree.cpp"
#endif

#ifndef HEADER_PRIME
#define HEADER_PRIME
    #include "../prime/prime_tools.cpp"
#endif

bool is_included(const std::vector<int>& v1, const std::vector<int>& v2){ // v1 included in v2
    return std::all_of(v1.cbegin(), v1.cend(), [&v2](int i){ return contains(v2, i); });
}

BinaryTree* new_tree_maker(const std::vector<PTE_Solution>& pte_solutions, std::vector<int> elements_in, std::vector<PTE_Solution> solutions){  // Classer les pte_solutions en fonction de leur élément maximal (au début) !
    auto* res = new BinaryTree;
    res->set_value(-1);
    std::vector<PTE_Solution> solutions_not_found;
    for (const auto& sol : pte_solutions){
        std::vector<int> elements;
        if (is_included(sol.elements, elements_in)){
            solutions.push_back(sol);
        }
        else {
            solutions_not_found.push_back(sol);
        }
    }
    if (solutions_not_found.empty()){
        res->set_solutions(solutions);
        return res;
    }
    size_t nb_elements = (solutions_not_found[solutions_not_found.size()-1]).maximum() + 1; // TODO: définir maxi (le dernier élément de A ou de B en fonction du degré
    auto *occurrences = (int *) malloc(sizeof(int) * nb_elements);
    for (int i = 0; i < nb_elements - 1; i++) {
        occurrences[i] = 0;
    }
    occurrences[nb_elements - 1] = '\0';
    for (auto sol : solutions_not_found){
        for (int j = 0; j < sol.n; j++){
            if (!contains(elements_in, sol.A[j])){
                occurrences[sol.A[j]]++;
            }
            if (!contains(elements_in, sol.B[j])){
                occurrences[sol.B[j]]++;
            }
        }
    }
    int most_common = 0;
    for (int i = 1; i < nb_elements; i++){
        if (occurrences[i] > occurrences[most_common]){
            most_common = i;
        }
    }
    res->set_value(most_common);
    std::vector<PTE_Solution> solutions_without;
    for (auto & pte_solution : pte_solutions){
        if (!contains(pte_solution.elements, most_common)){
            solutions_without.push_back(pte_solution);
        }
    }
    BinaryTree* right_child = new_tree_maker(solutions_without, elements_in, solutions);
    res->set_right_child(right_child);
    elements_in.push_back(most_common);
    BinaryTree* left_child = new_tree_maker(solutions_not_found, elements_in, solutions);
    res->set_left_child(left_child);
    return res;
}

big_int new_get_prime_from_l(const big_int* sieving, BinaryTree* tree, big_int l, big_int L){
    while (tree->get_value() != -1){
        int i = tree->get_value();
        if (l >= i and sieving[l-i]){
            tree = tree->get_left_child();
        }
        else{
            tree = tree->get_right_child();
        }
    }
    if (!tree->get_solutions().empty()){
        for (const auto& sol : tree->get_solutions()){
            big_int p = 2*sol.mC(l+L+1)/sol.C + 1;
            if ((sol.mC(l+L+1) % sol.C == 0) and (is_prime(p))){
                return p;
            }
        }
    }
    return 0;
}