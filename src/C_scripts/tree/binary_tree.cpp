#ifndef HEADER_PTE
#define HEADER_PTE
    #include "../pte_solutions/pte_solution.cpp"
#endif

class BinaryTree{
private:
    int _value{};
    BinaryTree *right_child{}, *left_child{};
    std::vector<PTE_Solution> _solutions;
public:
    void set_right_child(BinaryTree* node) {this->right_child = node;}
    void set_left_child(BinaryTree* node) {this->left_child = node;}
    void set_value(const int& value) {_value = value;}
    BinaryTree* get_right_child() {return this->right_child;}
    BinaryTree* get_left_child() {return this->left_child;}
    int get_value() const {return _value;}
    std::vector<PTE_Solution> get_solutions(){return _solutions;}
    void add_solution(const PTE_Solution& solution){
        _solutions.push_back(solution);
    }
    void set_solutions(const std::vector<PTE_Solution>& solutions){
        _solutions = solutions;
    }
};

/* TODO: remove it */
void parcourir_arbre(BinaryTree* tree, int depth){
    std::cout << tree->get_value() << " ";
    if (tree->get_right_child() != nullptr){
        parcourir_arbre(tree->get_right_child(), depth+1);
    }
    if (tree->get_left_child() != nullptr){
        parcourir_arbre(tree->get_left_child(), depth+1);
    }
    if (depth == 0){std::cout << "\n";}
}

/* TODO: remove it */
BinaryTree* treemake(int i){
    auto* nodeptr = new BinaryTree;
    nodeptr->set_value(i);
    if (i < 3){
        nodeptr->set_right_child(treemake(i+1));
    }
    return nodeptr;
}

/* TODO: remove it */
int get_size(BinaryTree* tree){
    if (tree->get_value() == -1){return 1;}
    else{
        return get_size(tree->get_left_child()) + get_size(tree->get_right_child());
    }
}