#ifndef HEADER_PTE
#define HEADER_PTE
    #include "../pte_solutions/pte_solution.cpp"
#endif

class OrdinaryTree{
private:
    int _value{};
    std::vector<OrdinaryTree*> _children;
    std::vector<PTE_Solution> _solutions;
public:
    void add_child(OrdinaryTree* child) {_children.push_back(child);}
    void set_value(const int& value) {_value = value;}
    std::vector<OrdinaryTree*> get_children() {return _children;}
    int get_value() const {return _value;}
    std::vector<PTE_Solution> get_solutions(){return _solutions;}
    void set_solutions(const std::vector<PTE_Solution>& solutions){
        _solutions = solutions;
    }
};

/* TODO: remove it */
void parcourir_arbre(OrdinaryTree* tree, int depth){
    std::cout << tree->get_value() << " ";
    if (!(tree->get_children()).empty()){
        for (auto child: (tree->get_children())){
            parcourir_arbre(child, depth+1);
        }
    }
    if (depth == 0){std::cout << "\n";}
}