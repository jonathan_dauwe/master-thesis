#include <ctime>
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include "prime_finder.cpp"
#include "sieving/original_method_sieving.cpp"
#include "pte_solutions/pte_solutions_example.cpp"



int main(){
    
    int B = 31;
    big_int L(20), R(517);
    int n = 4;
    big_int* sieving = b_smooth_sieving(B, L, R);

    std::vector<std::vector<PTE_Solution>> PTE_SOLUTIONS;

    std::vector<PTE_Solution> degree0_solutions;

    std::vector<PTE_Solution> degree1_solutions;

    std::vector<PTE_Solution> degree2_solutions;

    std::vector<PTE_Solution> degree3_solutions;

    int A_4_1[] = {0, 3, 4, 7}; int B_4_1[] = {1, 1, 6, 6};
    int A_4_3[] = {0, 5, 5, 10}; int B_4_3[] = {1, 2, 8, 9};
    int A_4_2[] = {0, 4, 7, 11}; int B_4_2[] = {1, 2, 9, 10};
    int A_4_5[] = {0, 6, 7, 13}; int B_4_5[] = {1, 3, 10, 12};
    int A_4_10[] = {0, 6, 8, 14}; int B_4_10[] = {2, 2, 12, 12};
    int A_4_4[] = {0, 5, 10, 15}; int B_4_4[] = {1, 3, 12, 14};
    int A_4_7[] = {0, 7, 9, 16}; int B_4_7[] = {1, 4, 12, 15};
    int A_4_9[] = {0, 5, 12, 17}; int B_4_9[] = {2, 2, 15, 15};
    int A_4_12[] = {0, 8, 9, 17}; int B_4_12[] = {2, 3, 14, 15};
    int A_4_11[] = {0, 7, 11, 18}; int B_4_11[] = {2, 3, 15, 16};
    int A_4_6[] = {0, 6, 13, 19}; int B_4_6[] = {1, 4, 15, 18};
    int A_4_8[] = {0, 8, 11, 19}; int B_4_8[] = {1, 5, 14, 18};

    std::vector<PTE_Solution> degree4_solutions;
    degree4_solutions.push_back(PTE_Solution(std::vector<int>(A_4_1, A_4_1+4), std::vector<int>(B_4_1, B_4_1+4)));
    degree4_solutions.push_back(PTE_Solution(std::vector<int>(A_4_3, A_4_3+4), std::vector<int>(B_4_3, B_4_3+4)));
    degree4_solutions.push_back(PTE_Solution(std::vector<int>(A_4_2, A_4_2+4), std::vector<int>(B_4_2, B_4_2+4)));
    degree4_solutions.push_back(PTE_Solution(std::vector<int>(A_4_5, A_4_5+4), std::vector<int>(B_4_5, B_4_5+4)));
    degree4_solutions.push_back(PTE_Solution(std::vector<int>(A_4_10, A_4_10+4), std::vector<int>(B_4_10, B_4_10+4)));
    degree4_solutions.push_back(PTE_Solution(std::vector<int>(A_4_4, A_4_4+4), std::vector<int>(B_4_4, B_4_4+4)));
    degree4_solutions.push_back(PTE_Solution(std::vector<int>(A_4_7, A_4_7+4), std::vector<int>(B_4_7, B_4_7+4)));
    degree4_solutions.push_back(PTE_Solution(std::vector<int>(A_4_9, A_4_9+4), std::vector<int>(B_4_9, B_4_9+4)));
    degree4_solutions.push_back(PTE_Solution(std::vector<int>(A_4_12, A_4_12+4), std::vector<int>(B_4_12, B_4_12+4)));
    degree4_solutions.push_back(PTE_Solution(std::vector<int>(A_4_11, A_4_11+4), std::vector<int>(B_4_11, B_4_11+4)));
    degree4_solutions.push_back(PTE_Solution(std::vector<int>(A_4_6, A_4_6+4), std::vector<int>(B_4_6, B_4_6+4)));
    degree4_solutions.push_back(PTE_Solution(std::vector<int>(A_4_8, A_4_8+4), std::vector<int>(B_4_8, B_4_8+4)));

    std::vector<PTE_Solution> degree5_solutions;
    
    std::vector<PTE_Solution> degree6_solutions;
    
    PTE_SOLUTIONS.push_back(degree0_solutions);
    PTE_SOLUTIONS.push_back(degree1_solutions);
    PTE_SOLUTIONS.push_back(degree2_solutions);
    PTE_SOLUTIONS.push_back(degree3_solutions);
    PTE_SOLUTIONS.push_back(degree4_solutions);
    PTE_SOLUTIONS.push_back(degree5_solutions);
    PTE_SOLUTIONS.push_back(degree6_solutions);

    std::vector<PTE_Solution> pte_solutions = PTE_SOLUTIONS[n];

    std::cout << derecursified_method_find_prime_number(sieving, L, R, pte_solutions) << " ";
    std::cout << new_method_find_prime_number(sieving, L, R, pte_solutions) << " ";
    std::cout << "\nDone";
}
