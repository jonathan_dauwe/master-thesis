#include "prime_numbers.cpp"

typedef unsigned long long int big_int;

class Prime{
public:
    big_int index = 0;
    static big_int first(){return prime_numbers[0];}
    big_int current() const{return prime_numbers[index];}
    big_int next(){return prime_numbers[++index];}
    big_int previous(){return prime_numbers[--index];}
};

/* TODO: choisir entre cette fonction et la suivante. */
/* Return true if an integer between 0 and 10^12 is prime.
 * bool is_prime(big_int n){
	if (n == 0 || n == 1){return false;}
	else{
		for (Prime p; p.current() * p.current() <= n; p.next()){
			if (n % p.current() == 0){
				return false;
			}
		}
	}
	return true;
}*/

bool is_prime(big_int n){
	if (n == 0 || n == 1){return false;}
	else if (n % 2 == 0){return n == 2;}
	else{
		for (big_int i = 3; i*i <= n; i = i+2){
			if (n%i == 0){
				return false;
			}
		}
	}
	return true;
}
