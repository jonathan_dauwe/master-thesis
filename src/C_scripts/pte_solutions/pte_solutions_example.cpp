#ifndef HEADER_PTE
#define HEADER_PTE
    #include "pte_solution.cpp"
#endif

/*
std::vector<PTE_Solution> degree0_solutions;

std::vector<PTE_Solution> degree1_solutions;

std::vector<PTE_Solution> degree2_solutions;

std::vector<PTE_Solution> degree3_solutions;

std::vector<PTE_Solution> degree4_solutions;

std::vector<PTE_Solution> degree5_solutions;

int A_6_1[] = {0, 3, 5, 11, 13, 16}; int B_6_1[] = {1, 1, 8, 8, 15, 15};
int A_6_2[] = {0, 5, 6, 16, 17, 22}; int B_6_2[] = {1, 2, 10, 12, 20, 21};
int A_6_3[] = {0, 4, 9, 17, 22, 26}; int B_6_3[] = {1, 2, 12, 14, 24, 25};
int A_6_4[] = {0, 7, 7, 21, 21, 28}; int B_6_4[] = {1, 3, 12, 16, 25, 27};
int A_6_5[] = {0, 7, 8, 22, 23, 30}; int B_6_5[] = {2, 2, 15, 15, 28, 28};
int A_6_7[] = {0, 8, 9, 25, 26, 34}; int B_6_7[] = {1, 4, 14, 20, 30, 33};
int A_6_6[] = {0, 5, 13, 23, 31, 36}; int B_6_6[] = {1, 3, 16, 20, 33, 35};
int A_6_8[] = {0, 7, 11, 25, 29, 36}; int B_6_8[] = {1, 4, 15, 21, 32, 35};
int A_6_10[] = {0, 8, 11, 27, 30, 38}; int B_6_10[] = {2, 3, 18, 20, 35, 36};
int A_6_9[] = {0, 9, 11, 29, 31, 40}; int B_6_9[] = {1, 5, 16, 24, 35, 39};
int A_6_11[] = {0, 5, 16, 26, 37, 42}; int B_6_11[] = {2, 2, 21, 21, 40, 40};
int A_6_13[] = {0, 7, 14, 28, 35, 42}; int B_6_13[] = {2, 3, 20, 22, 39, 40};
int A_6_12[] = {0, 6, 17, 29, 40, 46}; int B_6_12[] = {1, 4, 20, 26, 42, 45};
int A_6_14[] = {0, 10, 13, 33, 36, 46}; int B_6_14[] = {1, 6, 18, 28, 40, 45};
int A_6_15[] = {0, 9, 17, 34, 36, 46}; int B_6_15[] = {1, 6, 24, 25, 42, 44};
int A_6_16[] = {0, 9, 14, 32, 37, 46}; int B_6_16[] = {2, 4, 21, 25, 42, 44};
int A_6_17[] = {0, 9, 16, 34, 41, 50}; int B_6_17[] = {1, 6, 20, 30, 44, 49};
int A_6_20[] = {0, 12, 13, 37, 38, 50}; int B_6_20[] = {2, 5, 22, 28, 45, 48};
int A_6_18[] = {0, 11, 15, 37, 41, 52}; int B_6_18[] = {1, 7, 20, 32, 45, 51};
int A_6_19[] = {0, 7, 21, 35, 49, 56}; int B_6_19[] = {1, 5, 24, 32, 51, 55};
std::vector<PTE_Solution> degree6_solutions;
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_1, A_6_1+6), std::vector<int>(B_6_1, B_6_1+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_2, A_6_2+6), std::vector<int>(B_6_2, B_6_2+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_3, A_6_3+6), std::vector<int>(B_6_3, B_6_3+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_4, A_6_4+6), std::vector<int>(B_6_4, B_6_4+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_5, A_6_5+6), std::vector<int>(B_6_5, B_6_5+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_7, A_6_7+6), std::vector<int>(B_6_7, B_6_7+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_6, A_6_6+6), std::vector<int>(B_6_6, B_6_6+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_8, A_6_8+6), std::vector<int>(B_6_8, B_6_8+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_10, A_6_10+6), std::vector<int>(B_6_10, B_6_10+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_9, A_6_9+6), std::vector<int>(B_6_9, B_6_9+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_11, A_6_11+6), std::vector<int>(B_6_11, B_6_11+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_13, A_6_13+6), std::vector<int>(B_6_13, B_6_13+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_12, A_6_12+6), std::vector<int>(B_6_12, B_6_12+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_14, A_6_14+6), std::vector<int>(B_6_14, B_6_14+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_15, A_6_15+6), std::vector<int>(B_6_15, B_6_15+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_16, A_6_16+6), std::vector<int>(B_6_16, B_6_16+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_17, A_6_17+6), std::vector<int>(B_6_17, B_6_17+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_20, A_6_20+6), std::vector<int>(B_6_20, B_6_20+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_18, A_6_18+6), std::vector<int>(B_6_18, B_6_18+6)));
degree6_solutions.push_back(PTE_Solution(std::vector<int>(A_6_19, A_6_19+6), std::vector<int>(B_6_19, B_6_19+6)));
std::vector<std::vector<PTE_Solution>> PTE_SOLUTIONS;

PTE_SOLUTIONS.push_back(degree0_solutions);
PTE_SOLUTIONS.push_back(degree1_solutions);
PTE_SOLUTIONS.push_back(degree2_solutions);
PTE_SOLUTIONS.push_back(degree3_solutions);
PTE_SOLUTIONS.push_back(degree4_solutions);
PTE_SOLUTIONS.push_back(degree5_solutions);
PTE_SOLUTIONS.push_back(degree6_solutions);

std::vector<PTE_Solution> pte_solutions = PTE_SOLUTIONS[6];
*/