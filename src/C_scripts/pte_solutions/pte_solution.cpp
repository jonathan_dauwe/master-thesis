#ifndef HEADER_VECTOR
#define HEADER_VECTOR
#include "../vector_tools.cpp"
#endif

typedef unsigned long long int big_int;

/* TODO: use the same notations than in overleaf (reduced polynomial, ...). */
class PTE_Solution{
public:
    std::vector<int> A, B, elements; // A contains 0
    big_int n, C;
    big_int polynomial(big_int x, std::vector<int> v) const{
        big_int res = 1;
        for (big_int i = 0; i < n; i++) res *= x-v[i];
        return res;
    }
    big_int a(big_int x) const{return polynomial(x, this->A);}
    big_int b(big_int x) const{return polynomial(x, this->B);}
    big_int mC(big_int x) const{ // m*C TODO: call it smallest_polynomial
        if (this->n % 2 == 0){return this->a(x);}
        else {return this->b(x);}
    }
    int maximum() const {
        if (this->n % 2 == 0){return (this->A)[(this->n)-1];}
        else {return (this->B)[(this->n)-1];}
    }
    PTE_Solution(const std::vector<int>& A, const std::vector<int>& B){
        this->A = A, this->B = B, this->n = A.size();
        if (this->n % 2 == 0){this->C = this->b(0);}
        else {this->C = -this->b(0);}
        this->elements = disjoint_union(A, B);
    }
};

/* Return the element that appears in the most of the solutions and that is not already used. */
int most_common(const std::vector<PTE_Solution>& pte_solutions, const std::vector<int>& used_elements){
    size_t nb_elements = (pte_solutions[pte_solutions.size()-1]).maximum() + 1;
    // TODO: définir maxi (le dernier élément de A ou de B en fonction du degré) et classer les solutions par élément maximal
    auto *occurrences = (int *) malloc(sizeof(int) * nb_elements);
    for (int i = 0; i < nb_elements - 1; i++) {
        occurrences[i] = 0;
    }
    occurrences[nb_elements - 1] = '\0';
    for (const auto& sol : pte_solutions){
        for (int element : sol.elements){
            if (!contains(used_elements, element)){
                occurrences[element]++;
            }
        }
    }
    int most_common = 0;
    for (int i = 1; i < nb_elements; i++){
        if (occurrences[i] > occurrences[most_common]){
            most_common = i;
        }
    }
    return most_common;
}