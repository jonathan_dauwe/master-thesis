#include "tree/original_method_tree_maker.cpp"
#include "tree/derecursified_method_tree_maker.cpp"
#include "tree/new_method_tree_maker.cpp"

typedef unsigned long long int big_int;

big_int original_method_find_prime_number(big_int* sieving, big_int L, big_int R, const std::vector<PTE_Solution>& pte_solutions){
    OrdinaryTree* tree = original_tree_maker(pte_solutions);
    for (big_int l = 0; l < R-L; l++){
        big_int p = original_get_prime_from_l(sieving, tree, l, L);
        if (p != 0) {return p;}
    }
    return 0;
}

big_int derecursified_method_find_prime_number(big_int* sieving, big_int L, big_int R, const std::vector<PTE_Solution>& pte_solutions){
    BinaryTree* tree = derecursified_tree_maker(pte_solutions);
    for (big_int l = 0; l < R-L; l++){
        big_int p = derecursified_get_prime_from_l(sieving, tree, l, L);
        if (p != 0) {return p;}
    }
    return 0;
}

big_int new_method_find_prime_number(big_int* sieving, big_int L, big_int R, const std::vector<PTE_Solution>& pte_solutions){
    std::vector<int> elements_in;
    std::vector<PTE_Solution> solutions;
    BinaryTree* tree = new_tree_maker(pte_solutions, elements_in, solutions);
    for (big_int l = 0; l < R-L; l++){
        big_int p = new_get_prime_from_l(sieving, tree, l, L);
        if (p != 0) {return p;}
    }
    return 0;
}