#include <algorithm>
#include <cmath>
#ifndef HEADER_PRIME
#define HEADER_PRIME
    #include "../prime/prime_tools.cpp" // file containing the prime numbers lower than 100,000 in the variable prime_numbers.
#endif

typedef unsigned long long int big_int;

/* Return the floor of the logarithm of x in base b. */
int log(big_int x, big_int b){
    int res = 0;
    while (x > 1){
        x = x/b;
        res++;
    }
    return res;
}

/* TODO: diviser cette fonction et retirer la fonction d'affichage qui suit. */
/* Return an array of R-L elements in which the i-th element is true if L+i is B-smooth and false otherwise. */
big_int* b_smooth_sieving(int B, big_int L, big_int R) {
    size_t n = R - L;
    auto *res = (big_int *) malloc(sizeof(big_int) * n);
    for (int i = 0; i < n - 1; i++) {
        res[i] = 1;
    }
    res[n - 1] = '\0';
    Prime prime;
    big_int p = Prime::first();
    while (p < B) {
        for (int e = log(R, p); e > 0; e--) {
            big_int power = pow(p, e);
            for (big_int k = L + power - L % power; k < R; k += power) {
                if (k % (power * p) != 0) {
                    res[k - L - 1] *= power;
                }
            }
        }
        p = prime.next();
    }
    for (int i = 0; i < n - 1; i++) {
        res[i] = (res[i] == i + L + 1);
    }
    res[n - 1] = '\0';
    return res;
}