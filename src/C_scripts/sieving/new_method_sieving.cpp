#include <iostream> // std::cout, size_t, malloc
#include <vector> // std::vector, std::pair, std::make_pair
#include <tuple> // std::tie

typedef unsigned long long int big_int;

/* Create an array initialized with a defined value. */
template<typename T>
T* create_array(size_t size, T init_value){
    bool* array = (bool*) malloc(sizeof(bool) * (size));
    for (int i = 0; i < size; i++) array[i] = init_value;
    array[size] = '\0';
    return array;
}


/* Sieve the B-smooth numbers between L and R. */
bool* b_smooth_sieving(int B, big_int L, big_int R){
    bool* res = create_array(R-L+1, false);
    int nb_primes = 0;
    while (prime_numbers[nb_primes] <= B){nb_primes++;}
    std::vector<int> prime_base_indexes; // These elements are notated i and p = prime_numbers[i]
    for (int i = 0; i < nb_primes; i++){prime_base_indexes.push_back(i);}
    std::vector<big_int> prime_powers(prime_numbers, prime_numbers + nb_primes); // These elements are notated n = p^e
    while (!prime_base_indexes.empty()){
        std::vector<int> new_prime_base_indexes;
        std::vector<big_int> new_prime_powers;
        for (size_t i = 0; i < prime_base_indexes.size(); i++){
            int j = prime_base_indexes[i];
            big_int old_n = prime_powers[i];
            while (prime_numbers[j] < L/old_n and prime_numbers[j] <= B){
                new_prime_base_indexes.push_back(j);
                new_prime_powers.push_back(prime_numbers[j++]*old_n);
            }
            j = prime_base_indexes[i];
            while (prime_numbers[j] < L/old_n){j++;}
            while (prime_numbers[j] <= R/old_n and prime_numbers[j] <= B){
                new_prime_base_indexes.push_back(j);
                new_prime_powers.push_back(prime_numbers[j]*old_n);
                res[prime_numbers[j++]*old_n - L] = true;
            }
        }
        prime_base_indexes = new_prime_base_indexes;
        prime_powers = new_prime_powers;
    }
    return res;
}

