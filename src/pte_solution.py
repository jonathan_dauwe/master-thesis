from functools import reduce


class PTE_Solution:

    def __init__(self, A, B):
        self.n = len(A)
        for i in range(self.n):
            if sum(map(lambda x: x**i, A)) != sum(map(lambda x: x**i, B)):
                raise ValueError("({A}, {B}) is not a solution of the PTE because of the {i}-th power".format(A=A,B=B,i=i))
        self.A = A
        self.B = B
        self.elements = set(A+B)
        self.C = reduce(lambda x,y:x*y, A, 1) - reduce(lambda x,y:x*y, B, 1)
        if self.C < 0:
            self.A, self.B, self.C = self.B, self.A, -self.C
        self.a = lambda x:reduce(lambda y,z:y*(x-z),A,1)
        self.b = lambda x:reduce(lambda y,z:y*(x-z),B,1)
