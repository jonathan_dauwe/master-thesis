from math import log, ceil, floor
from sieving import b_smooth_sieving

many_pte_solution = True

if many_pte_solution:
    from multiple_pte_solutions import PTE_SOLUTIONS
    from multiple_pte_prime_research import find_prime_number

else:
    from unique_pte_solutions import PTE_SOLUTIONS
    from unique_pte_prime_research import find_prime_number

if __name__ == "__main__":

    # Parameters definition
    """
    We are looking for a prime number p such that:
    - (p-1)/2 =: m is B-smooth
    - m+1 = (p+1)/2 is B-smooth too.
    - m and m+1 are in the interval (2^240, 2^256)
    """

    M, N = 240, 256  # m must be in the interval (2^M, 2^N)
    B = 2 ** 16.1  # Smoothness bound (m and m+1 have to be B-smooth)
    n = 6  # Order of the PTE problem

    # PTE solutions definition

    pte_solutions = PTE_SOLUTIONS[n]
    if many_pte_solution:
        C_min = min(pte_solutions, key=lambda sol: sol.C).C
        C_max = max(pte_solutions, key=lambda sol: sol.C).C
        L = 2 ** floor(M + ceil(log(C_min, 2)))
        R = 2 ** ceil(N + floor(log(C_max, 2)))
    else:
        C = pte_solutions[0].C
        L = 2 ** floor(M + ceil(log(C, 2)))  # l > L => p = (2*a(l)+1)/C > M
        R = 2 ** ceil(N + floor(log(C, 2)))  # l < R => p = (2*a(l)+1)/C < N

    # Test values
    n = 2
    pte_solutions = PTE_SOLUTIONS[n]  # A = [0,2] ; B = [1,1] => C = 1
    B = 7
    L = 32
    R = 80  # p in (32**2, 80**2) = (1024, 6400)
    # result : p = 4049 (4048 = 2^4 * 11 * 23   &  4050 = 2 * 3^4 * 5^2)

    # Sieving computation

    sieving = b_smooth_sieving(B, L, R)  # B-smooth numbers in [2^L, 2^R]

    # Prime number research

    if many_pte_solution:
        p = find_prime_number(sieving, L, pte_solutions)
    else:
        p = find_prime_number(sieving, L, R, pte_solutions[0])
    print(p)