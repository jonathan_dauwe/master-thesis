import sympy
from prime import prime
from math import log

"""
There are 3 versions.
The first one is the shortest but the function is recursive.
    It only helps to understand how it could work but is not efficient.
The second one is the way described in the article and is easy to parallelize
    (we just need to divide the interval (L, R)). However, it is slower than
    the second function.
The last one is the fastest but requires a lot of memory (even if we
    parallelize it) and it is harder to fairly parallelize.
"""

def b_smooth_sieving2(B, L, R, n=1):
    """ Returns all the B-smooth numbers in [L, R). """
    # This version is the shortest one but the function is recursive.
    res = []
    if L <= n < R:
        res.append(n)
    for p in sympy.primerange(2, min(R/n, B+1)): # B is B-smooth
        res.extend(b_smooth_sieving(p, L, R, n*p))
            # Replacing B by p allowed us to avoid repetition in the result
    return res


def b_smooth_sieving3(B, L, R):
    """ Returns all the B-smooth numbers in (L, R). """
    # This version is based on the article and is easy to parallelized.
    # However it's slower than the 2 others.
    d = {} # dictionary containing the products (between 2^L and 2^R) of prime
            # numbers strictly lower than B. If the key is equal to the value,
            # then the key is B-smooth.
    p = prime.first() # p = 2
    while p < B:
        for e in range(int(log(R, p)), 0, -1): # Then p^e <= R
            i = p**e - L%(p**e)
            for k in range(L + p**e - L%(p**e), R, p**e):
                if k % (p**(e+1)) != 0:
                    if d.get(k):
                        d[k] *= p**e
                    else:
                        d[k] = p**e
        p = prime.next(p)
    return [n for n in filter(lambda x:d[x]==x,d)]

def b_smooth_sieving(B, L, R):
    """ Returns all the B-smooth numbers in [L, R). """
    # This version is the fastest one but is hard to parallelized.
    # Attention: it is an array of booleans. If the i-th element is True, it
    # means that L+i is B-smooth.
    res = [False]*(R-L)
    primes = list(prime.range(2, B+1))
    pre_res = list(zip(primes, primes))
    while pre_res:
        new_pre_res = []
        for p_pre, n_pre in pre_res:
            for p in prime.range(p_pre, min(L/n_pre, B+1)):
                # if p not in P:
                new_pre_res.append((p, p*n_pre))
            for p in prime.range(max(L/n_pre, p_pre), min(R/n_pre, B+1)):
                # if p not in P:
                new_pre_res.append((p, p*n_pre))
                res[p*n_pre-L] = True
        pre_res = new_pre_res
    return res
