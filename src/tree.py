class DecisionTree:
    def __init__(self, value):
        self.value = value
        self.right_child = []
        self.wrong_child = []

    def set_value(self, value):
        self.value = value

    def add_right_child(self, child):
        self.right_child = child

    def add_wrong_child(self, child):
        self.wrong_child = child

    def size(self):
        """ Returns the number of nodes. """
        if type(self.right_child) != DecisionTree or type(self.wrong_child) != DecisionTree:
            return 1
        return 1 + self.right_child.size() + self.wrong_child.size()

    def depth(self):
        """ Returns the depth of the tree: the maximal number of edges in a path
        linking this tree (i.e. linking this node, the root, with a leaf).
        """
        if type(self.right_child) != DecisionTree:
            right = 1
        else:
            right = 1 + self.right_child.depth()
        if type(self.wrong_child) != DecisionTree:
            wrong = 1
        else:
            wrong = 1 + self.wrong_child.depth()
        return max(right, wrong)
