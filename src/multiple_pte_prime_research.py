from collections import Counter
from tree import DecisionTree
import sympy


def decision_tree_maker(pte_solutions, elements_in={0}, elements_out=set()):
    """ Returns a decision tree that allowed us to check each value only once. """
    if all((sol.elements & elements_in == sol.elements for sol in pte_solutions)):
        return pte_solutions
    occurrences = sum(map(Counter, map(lambda sol: sol.elements-elements_in, pte_solutions)), Counter())
    most_frequent = occurrences.most_common(1)[0][0]
    tree = DecisionTree(most_frequent)
    solutions_without = [sol for sol in pte_solutions if most_frequent not in sol.elements]
    tree.add_right_child(decision_tree_maker(pte_solutions, elements_in | {most_frequent}, elements_out))
    tree.add_wrong_child(decision_tree_maker(solutions_without, elements_in, elements_out | {most_frequent}))
    return tree


def possible_solutions(sieving, decision_tree, l):
    """ Returns the PTE solutions for which a(l) and b(l) are B-smooth. """
    node = decision_tree
    while type(node) != list:
        i = node.value
        if l - i >= 0 and sieving[l - i]:
            node = node.right_child
        else:
            node = node.wrong_child
    return node


def find_prime_number(sieving, L, pte_solutions):
    """ Returns a prime number using the method described in the article "eprint.1283" with some improvements. """
    decision_tree = decision_tree_maker(pte_solutions)
    print("(Tree created)")
    for l in range(len(sieving)):
        if sieving[l]:
            plausible_solutions = possible_solutions(sieving, decision_tree, l)
            for solution in plausible_solutions:
                if solution.b(l + L) % solution.C == 0 and \
                        sympy.isprime(2 * solution.b(l + L) // solution.C + 1):
                    return 2 * solution.b(l + L) // solution.C + 1
    raise ValueError("Impossible to find a prime number with these arguments.")
