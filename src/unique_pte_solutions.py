from pte_solution import PTE_Solution

""" Here is a tuple where the i-th element is a list with 1 solution to a PTE problem of degree i. """

PTE_SOLUTIONS = (
    [   # Order 0
        PTE_Solution([], [])
        ],
    [   # Order 1
        PTE_Solution([1],[0])
        ],
    [   # Order 2
        PTE_Solution([1,1],[0,2])
        ],
    [   # Order 3
        PTE_Solution([0, 3, 3], [1, 1, 4])
        ],
    [   # Order 4
        PTE_Solution([0, 3, 4, 7], [1, 1, 6, 6])
        ],
    [   # Order 5
        PTE_Solution([0, 4, 8, 16, 17], [1, 2, 10, 14, 18])
         ],
    [   # Order 6
        PTE_Solution([0, 3, 5, 11, 13, 16], [1, 1, 8, 8, 15, 15])
        ]
    )
