from prime_numbers import PRIME_NUMBERS


def remove_prime_numbers(numbers):
    """ This function removes the prime numbers from the list PRIME_NUMBERS. """
    for n in numbers:
        if n in PRIME_NUMBERS:
            PRIME_NUMBERS.remove(n)


class Prime:

    def __init__(self):
        self.index = 0  # This object points the first prime number: 2.

    def this(self):
        return PRIME_NUMBERS[self.index]

    def next(self):
        """ Returns the next prime numbers in the list PRIME_NUMBERS. """
        self.index += 1
        return PRIME_NUMBERS[self.index]

    def previous(self):
        """ Returns the previous prime numbers in the list PRIME_NUMBERS. """
        self.index -= 1
        return PRIME_NUMBERS[self.index]

    def first(self):
        """ Returns the first prime numbers in the list PRIME_NUMBERS. """
        self.index = 0
        return PRIME_NUMBERS[self.index]

    def range(self, start, stop):
        """ Returns a list of all the prime numbers between start and stop from the list PRIME_NUMBERS. """
        res = []
        self.index = 0
        while PRIME_NUMBERS[self.index] < start:
            self.index += 1
        while PRIME_NUMBERS[self.index] < stop:
            res.append(PRIME_NUMBERS[self.index])
            self.index += 1
        return res


prime = Prime()
